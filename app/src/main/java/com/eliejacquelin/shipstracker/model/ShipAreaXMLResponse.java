package com.eliejacquelin.shipstracker.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * The model of the XML response obtained with the web-service
 * Created by Elie on 05/08/2014.
 */
@Root(name ="data",strict = false)
public class ShipAreaXMLResponse {
    @Element(name="SECTIONS")
    private SectionsXMLResponse sections;

    public List<ShipArea> getShipAresList() {
        return sections.getShipAreasList();
    }
}

