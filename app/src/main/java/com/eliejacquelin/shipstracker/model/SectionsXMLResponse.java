package com.eliejacquelin.shipstracker.model;

import com.eliejacquelin.shipstracker.model.ShipArea;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * The model of the section element in the XML response of the webservice
 * Created by Elie on 05/08/2014.
 */
@Root(strict=false)
public class SectionsXMLResponse {
    @ElementList(name="row",inline=true)
    private List<ShipArea> shipAreasList;

    public List<ShipArea> getShipAreasList() {
        return shipAreasList;
    }
}
