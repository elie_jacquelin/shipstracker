package com.eliejacquelin.shipstracker.ui;

import android.content.Context;

import com.eliejacquelin.shipstracker.R;
import com.eliejacquelin.shipstracker.model.Ship;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

/**
 * A custom cluster renderer to display custom marker instead of the default one
 * Created by Elie on 04/08/2014.
 */
public class ShipsClusterRenderer extends DefaultClusterRenderer<Ship> {
    public ShipsClusterRenderer(Context context, GoogleMap map, ClusterManager<Ship> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(Ship ship, MarkerOptions markerOptions) {
        //Change the Marker icon
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ship_icon))
        .anchor(0.5f,0.5f)
        .rotation(ship.getAngle());
    }
}
