package com.eliejacquelin.shipstracker.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.eliejacquelin.shipstracker.R;

public class SearchWebViewActivity extends Activity {

    private WebView webView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_web_view);
        ActionBar actionBar = getActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        progressBar = (ProgressBar) findViewById(R.id.search_webview_progress);
        webView = (WebView) findViewById(R.id.search_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new myWebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                progressBar.setProgress(progress);
            }
        });
        String keyword = getIntent().getStringExtra("keyword");
        //Load the result page corresponding to the keyword typed by the user
        webView.loadUrl("http://www.marinetraffic.com/fr/ais/index/search/all/keyword:" + keyword);
    }


    @Override
    public void onBackPressed() {
        //Allow the user to go back on the webview when pressing back
        if(webView.canGoBack()){
            webView.goBack();
        }else {
            super.onBackPressed();
        }
    }

    /**
     * A custom webview used only for cosmetic purposes
     */
    private class myWebViewClient extends WebViewClient{

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
            //Hide the webview to hide to the user the css modification when the page is loaded
            webView.setVisibility(View.INVISIBLE);
        }



        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            //Hide the get app on play store popup
            view.loadUrl("javascript:(function() {document.getElementsByClassName('bootbox modal fade getapp in')[0].style.display='none';})()");
            view.loadUrl("javascript:(function() {document.getElementsByClassName('modal-backdrop fade in')[0].style.display='none';})()");
            //Hide The ad which take half of the screen on phone
            view.loadUrl("javascript:(function() {document.getElementsByClassName('ads hidden-print vertical-offset-5')[0].style.display='none';})()");
            //Make the results take all screen size
            view.loadUrl("javascript:(function() {document.getElementsByClassName('container-fluid has-ad-space lists-page')[0].style.paddingRight='0px';})()");
            //Hide the banner
            view.loadUrl("javascript:(function() {document.getElementsByTagName('header')[0].style.display='none';})()");
            //Hide the footer
            view.loadUrl("javascript:(function() {document.getElementsByTagName('footer')[0].style.display='none';})()");
            //Make the details take all screen size
            view.loadUrl("javascript:(function() {document.getElementsByClassName('container-fluid has-ad-space detail-page')[0].style.paddingRight='0px';})()");
            //Hide the export data button
            view.loadUrl("javascript:(function() {document.getElementsByClassName('col-xs-6 col-sm-3 vertical-offset-10 pull-right text-right')[0].style.display='none';})()");
            //Display the webview with all the css changes made
            view.setVisibility(View.VISIBLE);
        }
    }
}
