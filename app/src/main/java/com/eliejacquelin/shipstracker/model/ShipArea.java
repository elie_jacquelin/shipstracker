package com.eliejacquelin.shipstracker.model;

import com.google.android.gms.maps.model.LatLng;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * The model of the ship area, created from the XML response of the webservice
 * Created by Elie on 05/08/2014.
 */
@Root(name="row")
public class ShipArea {

    @Attribute(name="SHIPCOUNT")
    private String shipsCount;
    @Attribute(name="Y")
    private float latitude;
    @Attribute(name="X")
    private float longitude;

    private LatLng position;

    @Override
    public String toString() {
        return "ShipArea{" +
                "shipsCount='" + shipsCount + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }

    public String getShipsCount() {
        return shipsCount;
    }

    public void setShipsCount(String shipsCount) {
        this.shipsCount = shipsCount;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public LatLng getPosition() {
        if(position == null) {
            position = new LatLng(latitude, longitude);
        }
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }
}
