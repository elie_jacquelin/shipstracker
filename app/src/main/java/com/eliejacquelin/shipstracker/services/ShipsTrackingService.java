package com.eliejacquelin.shipstracker.services;

import com.eliejacquelin.shipstracker.model.ShipAreaXMLResponse;
import com.eliejacquelin.shipstracker.model.ShipsTrackingResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * Interface of the webservices to used to track the ships
 * Created by Elie on 03/08/2014.
 */
public interface ShipsTrackingService {
    //Service used to display all the ships inside the bounds
    @Headers({

            "Referer: https://www.marinetraffic.com/en/",
            "Cookie: SERVERID=www4; _ga=GA1.2.542533011.1406653372; CAKEPHP=pnkh7dhu6k4ehmqatg8l7p71q7; firstLoadDone=true; maptype=roadmap; centerx=-70.2889259338379; centery=43.379971590090165; zoom=10; CheckFleet=false; CheckType6=true; CheckType7=true; CheckType8=true; CheckType4=true; CheckType3=true; CheckType9=true; CheckType2=true; CheckType1=true; CheckType0=true; CheckMoving=true; CheckStopped=true; CheckLegends=false; CheckWind=false; CheckPorts=false; CheckStations=false; CheckLights=false; CheckPhotos=false; CheckProjected=false; CheckPoints=false"
    })
    @GET("/map/getjson/sw_x:{swx}/sw_y:{swy}/ne_x:{nex}/ne_y:{ney}/zoom:{zoom}/fleet:/station:0")
    void getShips(@Path("swx") String swx, @Path("swy") String swy, @Path("nex") String nex, @Path("ney") String ney, @Path("zoom") String zoom, Callback<ShipsTrackingResponse> callback);

    //Service used to display the number of ships in an area
    @Headers({

            "Referer: https://www.marinetraffic.com/en/",
            "Cookie: SERVERID=www4; _ga=GA1.2.542533011.1406653372; CAKEPHP=pnkh7dhu6k4ehmqatg8l7p71q7; firstLoadDone=true; maptype=roadmap; centerx=-70.2889259338379; centery=43.379971590090165; zoom=6; CheckFleet=false; CheckType6=true; CheckType7=true; CheckType8=true; CheckType4=true; CheckType3=true; CheckType9=true; CheckType2=true; CheckType1=true; CheckType0=true; CheckMoving=true; CheckStopped=true; CheckLegends=false; CheckWind=false; CheckPorts=false; CheckStations=false; CheckLights=false; CheckPhotos=false; CheckProjected=false; CheckPoints=false"
    })
    @GET("/map/getsectionsxml_v3/sw_x:{swx}/sw_y:{swy}/ne_x:{nex}/ne_y:{ney}/zoom:{zoom}")
    void getShipsArea(@Path("swx") String swx, @Path("swy") String swy, @Path("nex") String nex, @Path("ney") String ney, @Path("zoom") String zoom, Callback<ShipAreaXMLResponse> callback);
}
