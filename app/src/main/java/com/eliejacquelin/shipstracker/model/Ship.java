package com.eliejacquelin.shipstracker.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * The bean of a ship
 * It can also be used to be drawn as cluster on the map
 * Created by Elie on 03/08/2014.
 */
public class Ship implements ClusterItem{
    private double latitude;
    private double longitude;
    private String name;
    private float angle;
    private LatLng position;

    public Ship(double latitude, double longitude, String name, float angle) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.angle = angle;
        position = new LatLng(latitude, longitude);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Used by the cluster manager on the map
    @Override
    public LatLng getPosition() {
        return position;
    }
}
