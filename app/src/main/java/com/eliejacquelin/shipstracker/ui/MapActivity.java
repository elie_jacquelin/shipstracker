package com.eliejacquelin.shipstracker.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import com.eliejacquelin.shipstracker.R;
import com.eliejacquelin.shipstracker.model.Ship;
import com.eliejacquelin.shipstracker.model.ShipArea;
import com.eliejacquelin.shipstracker.model.ShipAreaXMLResponse;
import com.eliejacquelin.shipstracker.model.ShipsTrackingResponse;
import com.eliejacquelin.shipstracker.services.ShipDeserializer;
import com.eliejacquelin.shipstracker.services.ShipsTrackingService;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.ui.IconGenerator;

import org.simpleframework.xml.core.Persister;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.converter.SimpleXMLConverter;


public class MapActivity extends Activity {

    private GoogleMap mMap;
    private ShipsTrackingService shipsTrackingServiceJSON;
    private ShipsTrackingService shipsTrackingServiceXML;
    private ShipTrackingServiceCallback shipTrackingServiceCallback;
    private ClusterManager<Ship> mClusterManager;
    private ShipAreaTrackingServiceCallback shipAreaTrackingServiceCallback;
    private Intent intentRefreshService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);
        //Get the map object
        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        //Initialize a cluster manager used to merge markers on the map
        mClusterManager = new ClusterManager<Ship>(this, mMap);
        //Set a custom renderer to change the marker visual
        mClusterManager.setRenderer(new ShipsClusterRenderer(this,mMap,mClusterManager));
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                //Notify the cluster manager of the camera change
                mClusterManager.onCameraChange(cameraPosition);
                //Refresh the ships position
                getShipsFromBounds();
            }
        });
        //Setup the webservice interface for the individual ship tracking
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ShipsTrackingResponse.class, new ShipDeserializer()) //Assign a custom deserializer for this class)
                .create();
        RestAdapter restAdapterJSON = new RestAdapter.Builder()
                .setEndpoint("https://www.marinetraffic.com")
                .setConverter(new GsonConverter(gson))
                .build();
        shipsTrackingServiceJSON = restAdapterJSON.create(ShipsTrackingService.class);

        //Setup the webservice interface for the ship arae tracking
        RestAdapter restAdapterXML = new RestAdapter.Builder()
                .setEndpoint("https://www.marinetraffic.com")
                .setConverter(new SimpleXMLConverter(new Persister()))
                .build();
        shipsTrackingServiceXML = restAdapterXML.create(ShipsTrackingService.class);
        //Create the corresponding callbacks
        shipTrackingServiceCallback = new ShipTrackingServiceCallback();
        shipAreaTrackingServiceCallback = new ShipAreaTrackingServiceCallback();
        //Create an handler running on the UI thread which will be used by a service to refresh the ships position
        Handler handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                //Refresh the ships position
                getShipsFromBounds();
            }
        };
        //Create the intent which will be send to the service
        intentRefreshService = new Intent(this, ShipRefreshService.class);
        intentRefreshService.putExtra("messenger", new Messenger(handler));

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Start the ships position refresh service when the activity is visible
        startService(intentRefreshService);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Stop the ships position refresh service when the activity is not visible
        stopService(intentRefreshService);
    }

    /**
     * Call the correct webservice according to the bounds and zoom level
     */
    private void getShipsFromBounds(){
        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        float zoom = mMap.getCameraPosition().zoom;
        String swx = String.valueOf(bounds.southwest.longitude);
        String swy = String.valueOf(bounds.southwest.latitude);
        String nex = String.valueOf(bounds.northeast.longitude);
        String ney = String.valueOf(bounds.northeast.latitude);
        String zoomS = String.valueOf(zoom);
        if(zoom > 6) {
            //Display individual ships
            shipsTrackingServiceJSON.getShips(swx, swy, nex, ney, zoomS, shipTrackingServiceCallback);
        }else{
            //Display area stating the number of ships present, zoom has to be truncate to make the webservice work correctly
            shipsTrackingServiceXML.getShipsArea(swx, swy, nex, ney, zoomS.substring(0,1), shipAreaTrackingServiceCallback);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        setSearchTextColourWithActionBarSherlock(searchView);
        searchView.setQueryHint("Vessel/Port name");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Intent intent = new Intent(getApplicationContext(), SearchWebViewActivity.class);
                intent.putExtra("keyword", s);
                startActivity(intent);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }

    private void setSearchTextColourWithActionBarSherlock(SearchView searchView) {
        int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        EditText searchPlate = (EditText) searchView.findViewById(searchPlateId);
        searchPlate.setTextColor(getResources().getColor(android.R.color.white));
        searchPlate.setHintTextColor(getResources().getColor(android.R.color.white));
        searchPlate.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_search || super.onOptionsItemSelected(item);
    }

    /**
     * The callback of the area ship tracking
     * Displays on the map icons containing the number of ships in this area
     */
    private class ShipAreaTrackingServiceCallback implements Callback<ShipAreaXMLResponse>{

        @Override
        public void success(ShipAreaXMLResponse shipAreas, Response response) {
            //Remove the previous markers
            mMap.clear();
            mClusterManager.clearItems();
            IconGenerator iconGenerator = new IconGenerator(getApplicationContext());
            for(ShipArea shipArea : shipAreas.getShipAresList()) {
                //Generate an icon displaying the nupber of ships in this area
                Bitmap icon = iconGenerator.makeIcon(shipArea.getShipsCount());
                mMap.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromBitmap(icon))
                                .position(shipArea.getPosition())
                );
            }
        }

        @Override
        public void failure(RetrofitError error) {
            error.printStackTrace();
            Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.network_error),Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * The callback of the individual ship tracking
     * Displays on the map cluster and markers of the individual ship
     */
    private class ShipTrackingServiceCallback implements Callback<ShipsTrackingResponse>{
        @Override
        public void success(ShipsTrackingResponse ships, retrofit.client.Response response) {
            //Remove the previous markers
            mMap.clear();
            mClusterManager.clearItems();
            //Add the markers on the map
            for (Ship ship : ships.getShips()) {
                //Display the ship on the map
                mClusterManager.addItem(ship);
            }
            //Force cluster to display new cluster even if the map is only panned and not zoomed
            mClusterManager.cluster();
        }

        @Override
        public void failure(RetrofitError error) {
            error.printStackTrace();
            Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.network_error),Toast.LENGTH_SHORT);
            toast.show();
        }
    }

//    private class ShipsUpdateReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(final Context context, Intent intent) {
//            MainActivity.this.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(context, "realtime update", Toast.LENGTH_SHORT).show();
//                    LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
//                    float zoom = mMap.getCameraPosition().zoom;
//                    getShipsFromBounds(bounds, zoom);
//
//                }
//            });
//        }
//    }


}
