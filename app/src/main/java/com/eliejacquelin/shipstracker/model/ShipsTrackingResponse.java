package com.eliejacquelin.shipstracker.model;

import com.eliejacquelin.shipstracker.model.Ship;

import java.util.List;

/**
 * Represent the response received from the ship tracking service
 * Created by Elie on 03/08/2014.
 */
public class ShipsTrackingResponse {
    private List<Ship> ships;

    public ShipsTrackingResponse(List<Ship> ships) {
        this.ships = ships;
    }

    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }
}
