package com.eliejacquelin.shipstracker.services;

import com.eliejacquelin.shipstracker.model.Ship;
import com.eliejacquelin.shipstracker.model.ShipsTrackingResponse;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Custom deserializer used to deserialize the json received when tracking ships
 * This class is used by GSON whenever the response is an ShipsTrackingResponse
 * Created by Elie on 03/08/2014.
 */
public class ShipDeserializer implements JsonDeserializer<ShipsTrackingResponse> {
    @Override
    public ShipsTrackingResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Ship> ships = new ArrayList<Ship>();
        JsonArray jsonArrayShips = (JsonArray) json; //The array of ships is at the root
        Iterator<JsonElement> iterator = jsonArrayShips.iterator();
        while (iterator.hasNext()) {
            JsonElement next = iterator.next();
            if(next.isJsonArray()) { //The next element is a ship which is also an array
                //Create a ship using only the first three parameters
                JsonArray jsonShip = (JsonArray) next;
                double latitude = jsonShip.get(0).getAsDouble();
                double longitude = jsonShip.get(1).getAsDouble();
                String name = jsonShip.get(2).getAsString();
                float angle = jsonShip.get(4).getAsFloat();
                Ship ship = new Ship(latitude, longitude, name,angle);
                ships.add(ship);
            }
        }
        return new ShipsTrackingResponse(ships);
    }
}
