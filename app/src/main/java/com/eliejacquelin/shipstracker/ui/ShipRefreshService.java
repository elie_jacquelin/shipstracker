package com.eliejacquelin.shipstracker.ui;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Service used to refresh the ships on real time
 * Created by Elie on 07/08/2014.
 */
public class ShipRefreshService extends IntentService {

    public ShipRefreshService(){
        super("ShipRefreshService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            final Messenger messenger = (Messenger) bundle.get("messenger");
            //Ask the ui thread of MainActivity to refresh the ships position every 10 seconds
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    //Create a new message and send it to the handler
                    Message msg = Message.obtain();
                    try {
                        messenger.send(msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            },5000, 10000);
        }
    }
}
