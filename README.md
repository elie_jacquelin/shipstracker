#ShipsTracker#

ShipsTracker is an application used to follow ships all around the world using the data from marinetraffic.com

### Build prerequisites ###
-The following libraries have to be downloaded from the SDK-manager :
	*Android Support Repository
	*Android Support Library
	*Google Play Services
	*Google Repository

Note : The others library required (retrofit, android-map-utils, simplexml) will be downloaded directly by gradle when compiling

- If you're on windows, go to the project root (ShipsTracker) and rename "gradlew.bat.REMOVE" to "gradlew.bat".
Gmail block emails containing .bat files

### Build with Android Studio ###
- Import the project
- Click on 'Sync Project with Gradle Files"
- Let gradle download libraries and compile the whole project
- Click on "Run 'app'" (the green arrow) to generate the APK
- The apk will be found under: ShipsTracker/app/build/outputs/apk/app-debug.apk

### Build using command line ###
- Open a terminal in the root of the project : ShipsTracker
- If you're on windows, type : "gradlew.bat assembleDebug"
- If you're on OSX or Linux, type : "chmod +x gradlew" and then "./gradlew assembleDebug"
- Let gradle download libraries  and compile the whole project 
- The apk will be found under: ShipsTracker/app/build/outputs/apk/app-debug.apk

